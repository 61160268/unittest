package unittest;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Game {
	private Scanner kb = new Scanner(System.in);
	private int row;
	private int col;
	Table table = null;
	Player o = null;
	Player x = null;

	public Game() {
		this.o = new Player('o');
		this.x = new Player('x');
	}

	public void run() {
		while (true) {
			this.runOnce();
			if (!askContinue()) {
				return;
			}
		}
	}

	private boolean askContinue() {
		while (true) {
			System.out.print("Continue y/n ? :");
			String ans = kb.next();
			if (ans.equals("n")) {
				return false;
			} else if (ans.equals("y")) {
				return true;
			}
		}

	}

	private int getRandomNumber(int min, int max) {
		return (int) ((Math.random() * (max - min)) + min);
	}

	public void newGame() {
		if (getRandomNumber(1, 100) % 2 == 0) {
			this.table = new Table(o, x);
		} else {
			this.table = new Table(x, o);
		}

	}

	public void runOnce() {
		this.showWelcome();
		this.newGame();
		while (true) {
			this.showTable();
			this.showTurn();
			this.inputRowCol();
			if (table.checkWin()) {
				this.showTable();
				this.showResult();
				this.showStat();
				return;
			}
			table.switchPlayer();
		}
	}

	public void showResult() {
		if (table.getWinner() != null) {
			showWin();
		} else {
			showDraw();
		}
	}

	public void showDraw() {
		System.out.println("Draw!!!");
	}

	public void showWin() {
		System.out.println(table.getWinner().getName() + " Win!!");
	}

	private void showStat() {
		System.out.println(o.getName() + "(win,lose,draw):" + o.getWin() + "," + o.getLose() + "," + o.getDraw());
		System.out.println(x.getName() + "(win,lose,draw):" + x.getWin() + "," + x.getLose() + "," + x.getDraw());
	}

	private void inputRowCol() {
		while (true) {
			this.input();
			try {
				if (table.setRowCol(row, col)) {
					return;
				}
			} catch (ArrayIndexOutOfBoundsException Exception) {
				System.out.println("Please input number 1-3");
			}

		}
	}

	public void showWelcome() {
		System.out.println("Welcom to OX Game");
	}

	public void showTable() {
		char[][] data = this.table.getData();
		for (int row = 0; row < data.length; row++) {
			System.out.print("|");
			for (int col = 0; col < data[row].length; col++) {
				System.out.print(" " + data[row][col] + " |");
			}
			System.out.println("");
		}
	}

	public void showTurn() {
		System.out.println("Turn " + table.getCurrentPlayer().getName());
	}

	public void input() {
		while (true) {
			try {
				System.out.print("Please input row and col : ");
				row = kb.nextInt();
				col = kb.nextInt();
				return;
			} catch (InputMismatchException Exception) {
				kb.next();
				System.out.println("Please input number 1-3");
			}
		}
	}
}