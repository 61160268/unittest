package unittest;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import unittest.Player;
import unittest.Table;

class TableUnitTest {

	@Test
	void testRow1Win() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o,x);
		table.setRowCol(1, 1);
		table.setRowCol(1, 2);
		table.setRowCol(1, 3);
		assertEquals(true,table.checkWin()) ;

	}
	void testRow2Win() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o,x);
		table.setRowCol(2, 1);
		table.setRowCol(2, 2);
		table.setRowCol(2, 3);
		assertEquals(true,table.checkWin()) ;

	}
	void testRow3Win() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o,x);
		table.setRowCol(3, 1);
		table.setRowCol(3, 2);
		table.setRowCol(3, 3);
		assertEquals(true,table.checkWin()) ;

	}
	void testCol1Win() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o,x);
		table.setRowCol(1, 1);
		table.setRowCol(2, 1);
		table.setRowCol(3, 1);
		assertEquals(true,table.checkWin()) ;
	}
	void testCol2Win() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o,x);
		table.setRowCol(1, 2);
		table.setRowCol(2, 2);
		table.setRowCol(3, 2);
		assertEquals(true,table.checkWin()) ;
	}
	void testCol3Win() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o,x);
		table.setRowCol(1, 3);
		table.setRowCol(2, 3);
		table.setRowCol(3, 3);
		assertEquals(true,table.checkWin()) ;
	}
	void testX1Win() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o,x);
		table.setRowCol(1, 1);
		table.setRowCol(2, 2);
		table.setRowCol(3, 3);
		assertEquals(true,table.checkWin());
	}
	void testX2Win() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o,x);
		table.setRowCol(1, 3);
		table.setRowCol(2, 2);
		table.setRowCol(3, 1);
		assertEquals(true,table.checkWin());
	}
	void testswitchPlayer() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o,x);
		table.switchPlayer();
		assertEquals('x',table.getCurrentPlayer().getName());
	
	}
	void testDraw() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o,x);
		table.switchPlayer();
		table.countTurn = 9;		
		assertEquals(true,table.checkWin());
	}
}
